### First upgrade

```sh
sudo apt update && sudo apt full-upgrade -y
```

### Optional: Disable activity status LED

See: https://www.jeffgeerling.com/blogs/jeff-geerling/controlling-pwr-act-leds-raspberry-pi

### Optional: Turn off Bluetooth

Update file `/boot/config.txt` and add following:

```
# Disable Bluetooth
dtoverlay=disable-bt
```

### Start Desktop with autologin at startup

Via `raspi-config`, set option `System Options` / `Boot  / Auto Login` / `Desktop autologin`.

### Enable VNC

Via `raspi-config`, set option `Interface Options` / `VNC` to `Yes`.

### Install project

From the **home folder** (this is important for the rest), run:

```sh
git clone https://gitlab.gnome.org/bo32/elpriser.git
cd elpriser
chmod u+x start.sh
python3 -m pip install -r requirements.txt -U
```

### Create startup script

```sh 
sudo cp /home/pi/elpriser/elpriser_back.service /etc/systemd/system/elpriser_back.service \
    && sudo systemctl enable elpriser_back.service


sudo cp /home/pi/elpriser/elpriser_ui.service /etc/systemd/system/elpriser_ui.service \
    && sudo systemctl enable elpriser_ui.service
```