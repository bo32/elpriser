from flask import Flask, render_template
from flask_socketio import SocketIO, emit
from flask_apscheduler import APScheduler

from services import prices as prices_helper
from services import dates as dates_helper

app = Flask(__name__)
socketio = SocketIO(app)

@app.route('/')
@app.route('/index')
def index():
    today, tomorrow = dates_helper.get_today_from_and_to()
    
    values = prices_helper.get_prices(today, tomorrow)
    print(values)

    return render_template('index.html', values=values)


scheduler = APScheduler()
scheduler.api_enabled = True
scheduler.init_app(app)

@scheduler.task('cron', id='refresh', minute='1')
def refresh_websocket():
    socketio.emit('refresh')
scheduler.start()

if __name__ == '__main__':
    socketio.run(app, allow_unsafe_werkzeug=True)