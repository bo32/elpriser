from datetime import datetime, timedelta
import requests
import shutil

from dateutil.parser import parse

from pathlib import Path
import csv

def get_prices(from_date, to_date):
    prices_file_url = f'https://andelenergi.dk/?obexport_format=csv&obexport_start={from_date}&obexport_end={to_date}&obexport_region=east'
    print(prices_file_url)
    response = requests.get(prices_file_url, stream=True)

    directory = Path('/tmp', 'elpriser')
    if not directory.is_dir():
        directory.mkdir()
    output_filpath=Path(directory, Path(f'{from_date}-{to_date}.csv'))
    with open(output_filpath, 'wb') as output_file:
        shutil.copyfileobj(response.raw, output_file)
    print('done downloading')

    now = datetime.now()

    with open(output_filpath, mode ='r') as file:
        csvFile = csv.reader(file)        
        is_first_line=True
        data = []
        for line in csvFile:
            if is_first_line:
                first_line=line
                is_first_line = False
            else:
                for index, value in enumerate(first_line):
                    if index != 0:
                        key = f'{line[0]} {value}'

                        as_time = parse(key)
                        as_time_format = "%Y-%m-%d %H:%M"
                        to_time_format = "%H:%M"
                        if (now  - timedelta(hours=4)) < as_time and (now  + timedelta(hours=20)) > as_time:
                            value = line[index].strip().replace(',', '.')
                            highlight = now > as_time and now < as_time + timedelta(hours=1) 
                            label = datetime.strptime(key, as_time_format).strftime(to_time_format)
                            
                            node = {'label': label, 'x': as_time.timestamp(), 'y': float(value), 'highlight': highlight}
                            
                            data.append(node)
    return data

