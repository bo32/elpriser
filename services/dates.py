from datetime import date, timedelta

def get_today():
    return date.today()

def get_today_from_and_to(delta = 1):
    today = get_today()
    return today, today + timedelta(days=delta)
